# terraform/main.tf
data "aws_caller_identity" "current" {}

locals {
    account_id  = data.aws_caller_identity.current.account_id
    bucket_name = "${local.account_id}-${var.region}-website-${var.environment}"
    tags_all = {
        Environment  = var.environment
        pipeline-url = var.pipeline_url
        CreationDate = timestamp()
        CreatedBy    = var.created_by
        Version      = var.deployment_version
    }
}

resource "aws_s3_bucket" "static-website" {
    bucket = local.bucket_name
    tags = local.tags_all
    acl    = "public-read"
    website {
        index_document = "index.html"
        error_document = "index.html"
    }

}

resource "aws_s3_bucket_policy" "public-read-policy" {
  bucket = aws_s3_bucket.static-website.id

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::${local.bucket_name}/*"
        }
    ]
}
POLICY
}