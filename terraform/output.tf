#terraform/outputs.tf
output "domain_name" {
    value    = aws_s3_bucket.static-website.bucket_domain_name
}

output "bucket_name" {
    value    = aws_s3_bucket.static-website.id
}


output "region" {
    value = var.region
}
